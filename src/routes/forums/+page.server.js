import { supabase } from '$lib/supabaseClient';
import { invalid } from '@sveltejs/kit';

/** @type {import('./$types').Actions} */
export const actions = {
	postForum: async ({ request }) => {
		const data = await request.formData();
		const name = data.get('posted_by');
		const title = data.get('title');
		const detail = data.get('detail');
		const { error } = await supabase.from('forum').insert([{ title, detail }]);

		if (error) {
			return invalid(500, error);
		}
	},
	postComments: async ({ request }) => {
		const data = await request.formData();
		const nama = data.get('posted_by');
		const title = data.get('title');
		const detail = data.get('detail');
		const { error } = await supabase.from('comments').insert([{ title, detail }]);

		if (error) {
			return invalid(500, error);
		}
	},
	deleteForum: async ({ request }) => {
		const data = await request.formData();
		const id = data.get('id');
		const { error } = await supabase.from('forum').delete().eq('id', id);
		if (error) {
			return invalid(500, error);
		}
	},
	voteUp: async ({ request }) => {
		const data = await request.formData();
		const id = data.get('id');
		const currentVote = await supabase.from('forum').select('votes').eq('id', id).single();
		console.log('currentVote', currentVote);
		const { error } = await supabase
			.from('forum')
			.update({ votes: currentVote.data?.votes + 1 })
			.eq('id', id);
		if (error) {
			return invalid(500, error);
		}
	},

	voteDown: async ({ request }) => {
		const data = await request.formData();
		const id = data.get('id');
		const currentVote = await supabase.from('forum').select('votes').eq('id', id).single();
		console.log('currentVote', currentVote);
		const { error } = await supabase
			.from('forum')
			.update({ votes: currentVote.data?.votes - 1 })
			.eq('id', id);
		if (error) {
			return invalid(500, error);
		}
	}
};
